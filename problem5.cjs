// Sort items based on number of Vitamins they contain.


function sortByNoOfVitamins(data){
    let sortByvitamins = data.sort((a,b)=>{
        let vitaminACount = a.contains.split(",").length
        let vitaminBCount = b.contains.split(",").length
        if(vitaminACount > vitaminBCount){
            return 1
        }else if (vitaminACount < vitaminBCount){
            return -1
        }
    })
    return sortByvitamins;

}

module.exports = sortByNoOfVitamins