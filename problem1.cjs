
//Get all items that are available 
function allItemsAvailable(data){
    let availableItems = data.filter((each)=>each.available == true)
    return availableItems
}

module.exports = allItemsAvailable