// Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }

function groupItems(data){
    const groupedItems = data.reduce((acc, curr) => {
        let ans = curr.contains.split(", ")
        ans.forEach(vitamin => {
          if (acc[vitamin]){
            acc[vitamin] = acc[vitamin]
          }else{
            acc[vitamin] =[]
          }
          acc[vitamin].push(curr.name);
        });
        return acc;
      }, {});
      return groupedItems
}


module.exports = groupItems
