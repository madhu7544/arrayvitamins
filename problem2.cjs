
//Get all items containing only Vitamin C.
function getAllVitaminC(data){
    let onlyVitaminC = data.filter((each)=>
        each.contains === 'Vitamin C')
    return onlyVitaminC
}

module.exports = getAllVitaminC