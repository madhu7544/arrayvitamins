

//Get all items containing Vitamin A.

function getVitaminA(data){
    let allVitaminAData = data.filter((each)=>{
        let vitamin = each.contains
        if (vitamin.includes('Vitamin A')){
            return each
        }
    })
    return allVitaminAData
}

module.exports = getVitaminA